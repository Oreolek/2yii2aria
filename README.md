2yii2aria
=========

This extension adds [aria2](https://github.com/aria2/aria2) remote control to the [Yii framework 2.0](http://www.yiiframework.com).

For license information check the [LICENSE](LICENSE.md)-file.

Requirements
------------
Tested on Yii 2.0.6. You'll need `aria2` installed too.

Usage
------------
### Run Aria2 in daemon mode

Run *Aria2* like this (you can read the manual about those options):

```
aria2c --enable-rpc -c -D
```

It is recommended to make it auto-start, so it would be always running.

### Usage

```
use oreolek\yii2aria2\Aria;

$server = array (
  'host'      => 'aria2.host.local' ,
  'port'      => '6800' ,
  'rpcsecret' => 'aria2rpcsecrettoken' ,
  'secure'    => TRUE ,
  'cacert'    => '/path/to/ca-cert/on/host/running/Aria2.pem' ,
  'proxy'     => array (
    'type' => 'socks5' ,
    'host' => 'localhost' ,
    'port' => 9050 ,
    'user' => 'proxyuser' ,
    'pass' => 'proxypassword'
  )
);

$aria2 = new Aria($server);
```

#### Secure RPC

If *Aria2* is configured for secure RPC, then set the `secure` key value in the `$server` array to `TRUE`.

If self-signed certificates are used, then the appropriate CA certificate will have to be copied over to the host running this library and its path specified in the `cacert` key value in the `$server` array.


#### RPC secret token

If *Aria2* has been configured with a secret token on the RPC interface then this should be specified in the `rpcsecret` key value in the `$server` array.

#### Connecting through proxy

*2yii2aria* allows connecting to the desired instance of *Aria2* via proxy. This can be accomplished by adding an sub-array item `proxy` to the array parameter passed to the class.

This sub-array associative and is used for setting the proxy configuration.

Please refer to the proxy configuration or documentation of your proxy service for information on values specific to the application environment.

It has the following elements.

##### Type

Use the value for the key `type` to specify the type of proxy to use. Currently only `http` and `socks5` types are supported.

There is no default value for this parameter.

##### Host

Use the value for the key `host` to specify the proxy host to use. Both IP addresses and resolvable host names are allowed.

There is no default value for this parameter.

##### Port

Use the value for the key `port` to specify the port of the proxy service. This is the port that the proxy service is listening for connections on.

There is no default value for this parameter.

###### Proxy authentication

If the proxy server requires authentication, then this can be achieved by two more items in the `proxy` sub-array.

Remember that this is different from the RPC authentication for *Aria2*. 

* Use the value for the key `user` to specify the username to authenticate with the proxy service.

  If `user` is not provided or is `NULL` then *2yii2aria* does not attempt to authenticate with the proxy service.
  
* Use the value for the key `pass` to specify the password to authenticate with the proxy service.

  If `pass` is not provided or is `NULL`, *2yii2aria* will use only the username to authenticate with the proxy service. While this scenario is unlikely, it allows connecting to proxy servers with non-standard setups.
  
  Proxy authentication with only username and without password depends on the proxy service and may fail if not configured properly.

Examples
----------

### Download a File

```
use oreolek\yii2aria2\Aria;

$aria2 = new Aria();

$addresult = $aria2->addUri ([
    'https://www.google.com.hk/images/srpr/logo3w.png'
  ],
  [
    'dir'=>'/tmp',
  ]
);
```
