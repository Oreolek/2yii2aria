<?php
namespace oreolek\yii2aria2;
/**
  * @package   2yii2aria
  * @brief     Library to communicate with aria2 using json-RPC
  * @detail    Provides a simple interface to communicate with Aria2
  *            the popular download manager, giving the flexibility to run
  *            it as a daemon and use it via the RPC interface.
  *            See README.md for more.
  * @licence   see LICENSE.md
  * @url       http://git.oreolek.ru/oreolek/2yii2aria
  **/

function setDefault($arr, $config, $value = NULL)
{
  if (
    !@array_key_exists ( $config, $arr ) |
    @is_null($arr[$config])
  )
  {
    $arr[$config] = $value;
  }
  return $arr;
}

class Aria {
  protected $server;
  protected $ch;

  function __construct
  (
    $server =
    [
      'host'      => '127.0.0.1',
      'port'      => 6800,
      'rpcsecret' => NULL,
      'secure'    => FALSE,
      'cacert'    => NULL,
      'proxy'     =>
      [
        'type' => NULL,
        'host' => NULL,
        'port' => NULL,
        'user' => NULL,
        'pass' => NULL
      ]
    ]
  )
  {
    \Yii::info('Aria2 server '.$server['host'].':'.$server['port'], __METHOD__);
    $this->server = $server;
    \Yii::trace('transferred host string to private class variable, now applying default values', __METHOD__);
    $this->server = setDefault($this->server, 'host', '127.0.0.1');
    $this->server = setDefault($this->server, 'port', '6800');
    $this->server = setDefault($this->server, 'rpcsecret');
    $this->server = setDefault($this->server, 'secure', FALSE);
    $this->server = setDefault($this->server, 'cacert');
    $this->server = setDefault($this->server, 'proxy');
    if (
      !is_null($this->server['proxy'])
    )
    {
      $this->server['proxy'] = setDefault($this->server['proxy'], 'type');
      if (
        !is_null($this->server['proxy']['type'])
      )
      {
        switch($this->server['proxy']['type']) {
        case 'http' :
          $this->server['proxy']['type'] = CURLPROXY_HTTP;
          break;
        case 'socks5' :
          $this->server['proxy']['type'] = CURLPROXY_SOCKS5;
          break;
        default :
          $this->server['proxy']['type'] = NULL;
        }
      }
      $this->server['proxy'] = setDefault($this->server['proxy'], 'host');
      $this->server['proxy'] = setDefault($this->server['proxy'], 'port');
      $this->server['proxy'] = setDefault($this->server['proxy'], 'user');
      $this->server['proxy'] = setDefault($this->server['proxy'], 'pass');
    }
    switch ($this->server['secure'])
    {
      case TRUE :
        \Yii::trace ( 'Secure RPC connection is requested; setting prefix for connection string');
        $connprefix = 'https://';
        $curlproto = CURLPROTO_HTTPS;
        break;
      default :
        \Yii::trace ( 'Secure RPC connection is not requested; setting prefix for connection string' );
        $connprefix = 'http://';
        $curlproto = CURLPROTO_HTTP;
    }
    \Yii::trace ( 'protocol selected for Connection prefix: '.$connprefix );
    \Yii::trace ( 'protocol selected for cURL: '.$curlproto, 'debug' );
    \Yii::trace ( 'connection prefix complete. Formulating connection string' );
    $connstring = $connprefix . $this->server['host'] . '/jsonrpc';
    \Yii::trace ( 'Connection string formulated, releasing prefix memory: '.$connstring);
    unset($connprefix);
    $this->ch = curl_init();
    \Yii::trace ('initiated curl; analysing errors');
    \Yii::trace ('error code:'.curl_errno($this->ch));
    \Yii::trace ('error message: '.curl_error($this->ch));

    $curlinfo = curl_version();
    \Yii::trace('Extracted cURL version info');
    \Yii::trace($curlinfo);
    \Yii::trace('Setting primary curl options');
    $result = NULL;
    \Yii::trace('initialised result buffer');
    \Yii::trace($result);
    $result = curl_setopt_array (
      $this->ch ,
      array (
        CURLOPT_POST              => TRUE ,
        CURLOPT_RETURNTRANSFER    => TRUE ,
        CURLOPT_HEADER            => FALSE ,
        CURLOPT_PROTOCOLS         => $curlproto ,
        CURLOPT_UNRESTRICTED_AUTH => FALSE ,
        CURLOPT_FOLLOWLOCATION    => FALSE ,
        CURLOPT_PORT              => $this->server['port'] ,
        CURLOPT_HTTPAUTH          => CURLAUTH_BASIC ,
        CURLOPT_USERAGENT         => 'Mozilla/5.0 (' . $curlinfo['host'] . ') libcURL/' . $curlinfo['version'] . ' 2yii2aria' ,
        CURLOPT_URL               => $connstring
      )
    );
    \Yii::trace( 'Curl options set; analysing errors ');
    \Yii::trace($result);
    \Yii::trace('error code: '.curl_errno($this->ch));
    \Yii::trace('error message: '.curl_error($this->ch));
    \Yii::trace('Freeing memory used to store cURL info: ');
    \Yii::trace($curlinfo);
    unset($curlinfo);
    \Yii::trace('Checking if CA certificate has been provided');
    \Yii::trace($this->server['cacert']);
    if (!is_null($this->server['cacert'])) {
      $result = NULL;
      \Yii::trace( 'initialised result buffer');
      \Yii::trace( $result );
      curl_setopt_array (
        $this->ch ,
        array (
          CURLOPT_CAINFO         => $this->server['cacert'] ,
          // CURLOPT_SSL_VERIFYPEER => FALSE , // WARNING: THIS OPTION IS INSECURE AND FOR TESTING ONLY
          // CURLOPT_SSL_VERIFYHOST => FALSE    // WARNING: THIS OPTION IS INSECURE AND FOR TESTING ONLY
        )
      );
      \Yii::trace ( 'CA cert for curl set; analysing errors ');
      \Yii::trace ( 'error code: '.curl_errno($this->ch) );
      \Yii::trace ( 'error message: '.curl_error($this->ch) );
    }
    \Yii::trace ( 'Checking curl proxy options' );
    if ( ! is_null($this->server['proxy']) ){
      if ( !is_null($this->server['proxy']['host']) && !is_null($this->server['proxy']['port']) ) {
        \Yii::trace ( 'Setting curl proxy options' );
        $result = NULL;
        \Yii::trace ( 'initialised result buffer' );
        $result = curl_setopt_array (
          $this->ch ,
          array (
            CURLOPT_PROXYTYPE       => $this->server['proxy']['type'] ,
            CURLOPT_PROXY           => $this->server['proxy']['host'] ,
            CURLOPT_PROXYPORT       => $this->server['proxy']['port'] ,
            CURLOPT_HTTPPROXYTUNNEL => FALSE ,
            CURLOPT_PROXYAUTH       => CURLAUTH_BASIC ,
            CURLOPT_PROXYUSERPWD    => $this->server['proxy']['user'] . ':' . $this->server['proxy']['pass']
          )
        );
        \Yii::trace( 'Curl options set; analysing errors ');
        \Yii::trace($result);
        \Yii::trace('error code: '.curl_errno($this->ch));
        \Yii::trace('error message: '.curl_error($this->ch));
        if ( !is_null($this->server['proxy']['user']) ) {
          \Yii::trace ( 'Non-null user; setting curl proxy login string' );
          $proxylogin = NULL; //first ensure you start with an empty string
          $proxylogin = $this->server['proxy']['user'];
          \Yii::trace ( 'curl proxy login string set' );
          \Yii::trace ( 'checking curl proxy password' );
          if ( !is_null($this->server['proxy']['pass']) ) {
            \Yii::trace ( 'Non-null password; setting curl proxy login string' );
            $proxylogin .= ':' . $this->server['proxy']['pass'];
            \Yii::trace ( 'curl proxy login string set' );
          } else {
            \Yii::trace ( 'Null password; ignoring password in curl proxy login setting' );
          }
          \Yii::trace ( 'Non-null user; setting curl proxy user' );
          $result = NULL;
          \Yii::trace ( 'initialised result buffer' );
          $result = curl_setopt ( $this->ch , CURLOPT_PROXYUSERPWD , $proxylogin );
          \Yii::trace( 'Curl options set; analysing errors ');
          \Yii::trace($result);
          \Yii::trace('error code: '.curl_errno($this->ch));
          \Yii::trace('error message: '.curl_error($this->ch));
          \Yii::trace ( 'Proxy login string used, releasing variable  memory' );
          unset($proxylogin);
        } else {
          \Yii::trace ( 'Null user; ignoring curl proxy login setting' );
        }
      } else {
        \Yii::trace ( 'Null host/port in proxy settings array' );
      }
    } else {
      \Yii::trace ( 'Null proxy settings array' );
    }
  }

  function __destruct() {
    \Yii::trace ( 'closing connection' , $this->ch );
    curl_close($this->ch);
    @\Yii::trace ( 'Checking if a curl log file is still open' , $logfile );
    if(@$logfile) {
      \Yii::trace ('curl log file is still open, now closing');
      if(fclose($logfile)) {
        \Yii::trace ('curl log file closed successfully');
      } else {
        \Yii::trace ('failed to close curl log file');
      }
    } else {
      \Yii::trace ( 'Log file pointer not found, ignoring' );
    }
  }

  private function req($data) {
    \Yii::trace ( 'formulating request via postfields' , $data );
    curl_setopt ( $this->ch , CURLOPT_POSTFIELDS , $data );
    \Yii::trace ( 'postfields formulation error code' , curl_errno($this->ch) );
    \Yii::trace ( 'postfields formulation error message' , curl_error($this->ch) );
    $result = NULL;
    \Yii::trace ( 'initialised result buffer' , $result );
    $result = curl_exec($this->ch);
    \Yii::trace ( 'request sent, analysing errors' , $result );
    \Yii::trace ( 'postfields formulation error code' , curl_errno($this->ch) );
    \Yii::trace ( 'postfields formulation error message' , curl_error($this->ch) );

    return $result;
  }

  function __call ( $name , $arg ) {
    \Yii::trace ( 'function called: '.$name );
    \Yii::trace ( 'arguments called' );
    \Yii::trace ( $arg );
    \Yii::trace ( 'checking for RPC secret' );
    if ( ! $this->server['rpcsecret'] == NULL ) {
      \Yii::trace ( 'Non-null RPC secret value, pre-pending to supplied arguments' , $this->server['rpcsecret'] , '' );
      array_unshift (
        $arg ,
        'token:' . $this->server['rpcsecret']
      );
    }
    $data = [
      'jsonrpc' => '2.0',
      'id'      => '1',
      'method'  => 'aria2.'.$name,
      'params'  => $arg
    ];
    \Yii::trace ( 'array formulated' );
    $data = json_encode($data);
    \Yii::trace ( 'array encoded to json, sending request' );
    $result = NULL;
    \Yii::trace ( 'initialised result buffer' );
    $result = $this->req($data);
    \Yii::trace ( 'response received' );
    \Yii::trace ( 'Checking for success' );
    if ( $result === FALSE ) {
      \Yii::trace ( 'curl failed' , $result );
    } else {
      \Yii::trace ( 'curl response valid, decoding json' );
      $result = json_decode ( $result , 1 );
      \Yii::trace ( 'decoded json, returning result' );
    }
    return $result;
  }
}
