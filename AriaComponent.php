<?php
namespace oreolek\yii2aria2;
use Yii;
use yii\base\Component;
use yii\base\ErrorException;
use oreolek\yii2aria2\Aria;

class AriaComponent extends Component {
      /**
       * @var  string  aria2 host (IP)
       */
      public $host = '127.0.0.1';

      /**
       * @var  integer  aria2 port
       */
      public $port = 6800;

      /**
       * @var  string  aria2 secret
       */
      public $rpcsecret = NULL;

      /**
       * @var  boolean  use HTTPS on RPC connections or not
       */
      public $secure = FALSE;

      /**
       * @var  string  path to CA cert
       */
      public $cacert = NULL;

      /**
       * @var  array  proxy options: type, host, port, user, pass
       */
      public $proxy = NULL;

      /**
       * Loads the image to Kohana_Image object
       * @param string $file the file path to the image
       * @param string $driver the image driver to use: GD or ImageMagick
       * @throws ErrorException if filename is empty or file doesn't exist
       * @return mixed object Image_GD or object Image_Imagick 
       */
      public function connect()
      {
            $options = [];
            if ($this->host) {
                  $options['host'] = $this->host;
            }
            if ($this->port) {
                  $options['port'] = $this->port;
            }
            if ($this->rpcsecret) {
                  $options['rpcsecret'] = $this->rpcsecret;
            }
            if ($this->secure) {
                  $options['secure'] = $this->secure;
            }
            if ($this->cacert) {
                  $options['cacert'] = $this->cacert;
            }
            if ($this->proxy) {
                  $options['proxy'] = $this->proxy;
            }
            $aria = new Aria($options);
            return $aria;
      }
}
